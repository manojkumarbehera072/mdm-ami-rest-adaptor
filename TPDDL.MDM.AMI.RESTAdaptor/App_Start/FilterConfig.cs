﻿using System.Web;
using System.Web.Mvc;

namespace TPDDL.MDM.AMI.RESTAdaptor
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
