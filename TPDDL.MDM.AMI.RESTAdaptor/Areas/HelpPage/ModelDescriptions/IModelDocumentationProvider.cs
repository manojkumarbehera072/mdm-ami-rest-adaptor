using System;
using System.Reflection;

namespace TPDDL.MDM.AMI.RESTAdaptor.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}