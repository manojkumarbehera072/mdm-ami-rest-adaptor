﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.MDM.AMI.RESTAdaptor.Models;

namespace TPDDL.MDM.AMI.RESTAdaptor.Helper
{
    public class UtilitiesDeviceERPSmartMeterCreateRequestHelper
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        HES hes = new HES();
        public void UtilitiesDeviceERPSmartMeterCreateRequest_Out(UtilsDvceERPSmrtMtrCrteReqMsg utilitiesDeviceERPSmartMeterCreateRequest)
        {
            try
            {
                string uuid = "";
                string CreatedBy = "";

                //CIS Log entry
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader != null)
                {
                    if (utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.UUID.Value;
                        uuid = utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.UUID.Value;
                    }
                    else
                    {
                        uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    if (utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.SenderBusinessSystemID != null)
                    {
                        CreatedBy = utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.SenderBusinessSystemID;
                    }
                    cISAdaptorParent.LogDateTime = utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.CreationDateTime;
                }
                else
                {
                    uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                cISAdaptorParent.Request_Type = "UtilitiesDeviceERPSmartMeterCreateRequest_Out";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);

                string fields = "";
                string values = "";
                string equipmentUtilityId = "";
                string updateEquipmentFields = "";
                //Validations
                if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice != null)
                {
                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.ID != null
                        && utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.ID.Value != null)
                    {
                        fields += "Utility_ID,";
                        values += utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.ID.Value + ",";
                        equipmentUtilityId = utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.ID.Value;
                    }
                    if (!String.IsNullOrEmpty(utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SerialID))
                    {
                        fields += "Meter_ID,";
                        values += utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SerialID + ",";
                        updateEquipmentFields += "Meter_ID =" + "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SerialID + "',";
                    }
                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.StartDate != DateTime.MinValue)
                    {
                        fields += "StartDate,";
                        values += "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.StartDate.ToString("yyyy-MM-dd") + "',";
                        updateEquipmentFields += "StartDate =" + "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.StartDate.ToString("yyyy-MM-dd") + "',";
                    }
                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.EndDate != DateTime.MinValue)
                    {
                        fields += "EndDate,";
                        values += "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.EndDate.ToString("yyyy-MM-dd") + "',";
                        updateEquipmentFields += "EndDate =" + "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.EndDate.ToString("yyyy-MM-dd") + "',";
                    }
                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.MaterialID != null
                        && utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.MaterialID.Value != null)
                    {
                        fields += "Material_ID,";
                        values += utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.MaterialID.Value + ",";
                        updateEquipmentFields += "Material_ID =" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.MaterialID.Value + ",";
                    }

                    // IndividualMaterialManufacturerInformation node
                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation != null)
                    {
                        if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID != null
                            && utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value != null)
                        {
                            fields += "Manufacturer_ID,";
                            values += "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value + "',";
                            updateEquipmentFields += "Manufacturer_ID =" + "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value + "',";
                        }
                        if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID != null
                        && utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID.Value != null)
                        {
                            fields += "Model_ID,";
                            values += utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID.Value + ",";
                            updateEquipmentFields += "Model_ID =" + "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID.Value + "',";
                        }
                        if (!String.IsNullOrEmpty(utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.SerialID))
                        {
                            fields += "Name_Plate_ID,";
                            values += utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.SerialID + ",";
                            updateEquipmentFields += "Name_Plate_ID =" + "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.SerialID + "',";
                        }
                    }

                    //SmartMeter node
                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SmartMeter != null)
                    {
                        if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID != null
                            && utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value != null)
                        {
                            //Get HES Code
                            int hesCode = hes.GetHESIdByHESCode(utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value);
                            fields += "HES_ID,";
                            values += hesCode + ",";
                            updateEquipmentFields += "HES_ID =" + "'" + hesCode + "',";
                        }
                    }
                    if (CreatedBy != null)
                    {
                        fields += "Created_by,";
                        values += "'" + CreatedBy + "',";
                    }
                    fields += "Created_on,";
                    values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                    if (CreatedBy != null)
                    {
                        updateEquipmentFields += "Changed_by = '" + CreatedBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                    }

                    //Remove end Comma 
                    fields = fields.TrimEnd(',');
                    values = values.TrimEnd(',');
                    updateEquipmentFields = updateEquipmentFields.TrimEnd(',');

                    Equipment equipment = new Equipment();
                    int equipId = equipment.GetEquipmentIdByUtilityId(equipmentUtilityId);

                    //UtilityId exists in equipment table
                    if (equipId > 0)
                    {
                        //update equipment
                        using (SqlConnection connection = new SqlConnection(constr))
                        {
                            connection.Open();
                            string query = "update equipment set " + updateEquipmentFields + " where Utility_ID='" + equipmentUtilityId + "'";
                            SqlCommand cmd = new SqlCommand(query, connection);
                            cmd.ExecuteNonQuery();
                            connection.Close();
                        }
                    }
                    else
                    {
                        //Equipment Entry
                        using (SqlConnection connection = new SqlConnection(constr))
                        {
                            connection.Open();
                            string query = "insert into equipment (" + fields + ") values(" + values + ")";
                            SqlCommand cmd = new SqlCommand(query, connection);
                            cmd.ExecuteNonQuery();
                            connection.Close();
                        }
                    }
                }
                //Write to File
                WriteToFile writeToFile = new WriteToFile();
                var xmlString = writeToFile.Serialize<UtilsDvceERPSmrtMtrCrteReqMsg>(utilitiesDeviceERPSmartMeterCreateRequest);
                writeToFile.WriteToLog(xmlString, uuid, "~/CISAdapterLog/");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}