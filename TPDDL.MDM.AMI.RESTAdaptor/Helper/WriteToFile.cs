﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace TPDDL.MDM.AMI.RESTAdaptor.Helper
{
    public class WriteToFile
    {
        public string Serialize<T>(T dataToSerialize)
        {
            try
            {
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void WriteToLog(string xmlStrings, string uuid, string path)
        {
            string fileNames = uuid + ".txt";
            string roots = System.Web.HttpContext.Current.Server.MapPath(@path);
            if (!Directory.Exists(roots))
            {
                Directory.CreateDirectory(roots);
            }
            string subdirs = roots + DateTime.Today.ToString("yyyy-MM-dd");
            if (!Directory.Exists(subdirs))
            {
                Directory.CreateDirectory(subdirs);
            }
            File.WriteAllText(subdirs + "/" + fileNames, xmlStrings);
        }
    }
}