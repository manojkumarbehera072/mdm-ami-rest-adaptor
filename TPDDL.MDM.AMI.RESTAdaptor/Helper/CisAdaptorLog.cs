﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.MDM.AMI.RESTAdaptor.Models;

namespace TPDDL.MDM.AMI.RESTAdaptor.Helper
{
    public class CisAdaptorLog
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;

        public void CISAdaptorLog(CISAdaptorParent cISAdaptorParent)
        {
            string fields = "";
            string values = "";

            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    if (cISAdaptorParent != null)
                    {
                        //validations
                        if (!String.IsNullOrEmpty(cISAdaptorParent.UUID))
                        {
                            fields += "UUID,";
                            values += "'" + cISAdaptorParent.UUID + "',";
                        }
                        if (cISAdaptorParent.LogDateTime != DateTime.MinValue)
                        {
                            fields += "Log_DateTime,";
                            values += "'" + cISAdaptorParent.LogDateTime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        }
                        if (!String.IsNullOrEmpty(cISAdaptorParent.Request_Type))
                        {
                            fields += "Request_Type,";
                            values += "'" + cISAdaptorParent.Request_Type + "',";
                        }
                        if (cISAdaptorParent.Child_Count != 0)
                        {
                            fields += "Child_Count,";
                            values += "'" + cISAdaptorParent.Child_Count + "',";
                        }
                        //Remove end Comma 
                        fields = fields.TrimEnd(',');
                        values = values.TrimEnd(',');

                        connection.Open();
                        string query = "insert into CIS_ADAPTOR_LOG (" + fields + ") values(" + values + ")";
                        SqlCommand cmd = new SqlCommand(query, connection);
                        cmd.ExecuteNonQuery();

                        connection.Close();
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CISAdaptorChildLog(CISAdaptorChild cISAdaptorChild)
        {
            string fields = "";
            string values = "";

            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    if (cISAdaptorChild != null)
                    {
                        if (!String.IsNullOrEmpty(cISAdaptorChild.Parent_UUID))
                        {
                            fields += "Parent_UUID,";
                            values += "'" + cISAdaptorChild.Parent_UUID + "',";
                        }
                        //validations
                        if (!String.IsNullOrEmpty(cISAdaptorChild.UUID))
                        {
                            fields += "UUID,";
                            values += "'" + cISAdaptorChild.UUID + "',";
                        }
                        if (cISAdaptorChild.LogDateTime != DateTime.MinValue)
                        {
                            fields += "Log_DateTime,";
                            values += "'" + cISAdaptorChild.LogDateTime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        }
                        if (!String.IsNullOrEmpty(cISAdaptorChild.Request_Type))
                        {
                            fields += "Request_Type,";
                            values += "'" + cISAdaptorChild.Request_Type + "',";
                        }

                        //Remove end Comma 
                        fields = fields.TrimEnd(',');
                        values = values.TrimEnd(',');

                        connection.Open();
                        string query = "insert into CIS_ADAPTOR_CHILD_LOG (" + fields + ") values(" + values + ")";
                        SqlCommand cmd = new SqlCommand(query, connection);
                        cmd.ExecuteNonQuery();

                        connection.Close();
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}