﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TPDDL.MDM.AMI.RESTAdaptor.Helper
{
    public class HES
    {
        SqlDataReader reader;
        SqlDataAdapter Adp;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;

        public int GetHESIdByHESCode(string hesName)
        {
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = "select id from hes_master where hes_cd='" + hesName + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["id"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetSAPAMIUrl(string servicename)
        {
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select ServiceUrl,UserId,Password from SAP_AMI_ADAPTOR_URL where upper(ServiceName)=upper('" + servicename + "')";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    DataTable dturl = new DataTable();
                    dturl.Clear();
                    Adp = new SqlDataAdapter();
                    Adp.SelectCommand = cmd;
                    Adp.Fill(dturl);

                    connection.Close();
                    return dturl;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}