﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace TPDDL.MDM.AMI.RESTAdaptor.Helper
{
    public class Equipment
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;

        public int GetEquipmentIdByUtilityId(string utilityId)
        {
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = "select ID from Equipment where Utility_ID='" + utilityId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["ID"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}