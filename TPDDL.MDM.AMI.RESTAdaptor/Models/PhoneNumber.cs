﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class PhoneNumber
    {
        private string areaIDField;
        private string subscriberIDField;
        private string extensionIDField;
        private string countryCodeField;
        private string countryDiallingCodeField;
        private MEDIUM_Name countryNameField;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string AreaID
        {
            get =>
                this.areaIDField;
            set
            {
                this.areaIDField = value;
                this.RaisePropertyChanged("AreaID");
            }
        }

        public string SubscriberID
        {
            get =>
                this.subscriberIDField;
            set
            {
                this.subscriberIDField = value;
                this.RaisePropertyChanged("SubscriberID");
            }
        }

        public string ExtensionID
        {
            get =>
                this.extensionIDField;
            set
            {
                this.extensionIDField = value;
                this.RaisePropertyChanged("ExtensionID");
            }
        }

        public string CountryCode
        {
            get =>
                this.countryCodeField;
            set
            {
                this.countryCodeField = value;
                this.RaisePropertyChanged("CountryCode");
            }
        }

        public string CountryDiallingCode
        {
            get =>
                this.countryDiallingCodeField;
            set
            {
                this.countryDiallingCodeField = value;
                this.RaisePropertyChanged("CountryDiallingCode");
            }
        }

        public MEDIUM_Name CountryName
        {
            get =>
                this.countryNameField;
            set
            {
                this.countryNameField = value;
                this.RaisePropertyChanged("CountryName");
            }
        }
    }
}

