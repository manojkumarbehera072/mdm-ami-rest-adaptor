﻿using System.ComponentModel;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class BusinessScope
    {
        private BusinessScopeTypeCode typeCodeField;
        private BusinessScopeInstanceID instanceIDField;
        private BusinessScopeID idField;
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public BusinessScopeTypeCode TypeCode
        {
            get =>
                this.typeCodeField;
            set
            {
                this.typeCodeField = value;
                this.RaisePropertyChanged("TypeCode");
            }
        }

        public BusinessScopeInstanceID InstanceID
        {
            get =>
                this.instanceIDField;
            set
            {
                this.instanceIDField = value;
                this.RaisePropertyChanged("InstanceID");
            }
        }

        public BusinessScopeID ID
        {
            get =>
                this.idField;
            set
            {
                this.idField = value;
                this.RaisePropertyChanged("ID");
            }
        }
    }
}

