﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class BusinessDocumentMessageHeaderPartyContactPerson
    {
        private ContactPersonInternalID internalIDField;
        private string[] organisationFormattedNameField;
        private string[] personFormattedNameField;
        private PhoneNumber[] phoneNumberField;
        private PhoneNumber[] faxNumberField;
        private EmailURI[] emailURIField;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public ContactPersonInternalID InternalID
        {
            get =>
                this.internalIDField;
            set
            {
                this.internalIDField = value;
                this.RaisePropertyChanged("InternalID");
            }
        }

        public string[] OrganisationFormattedName
        {
            get =>
                this.organisationFormattedNameField;
            set
            {
                this.organisationFormattedNameField = value;
                this.RaisePropertyChanged("OrganisationFormattedName");
            }
        }

        public string[] PersonFormattedName
        {
            get =>
                this.personFormattedNameField;
            set
            {
                this.personFormattedNameField = value;
                this.RaisePropertyChanged("PersonFormattedName");
            }
        }

        public PhoneNumber[] PhoneNumber
        {
            get =>
                this.phoneNumberField;
            set
            {
                this.phoneNumberField = value;
                this.RaisePropertyChanged("PhoneNumber");
            }
        }

        public PhoneNumber[] FaxNumber
        {
            get =>
                this.faxNumberField;
            set
            {
                this.faxNumberField = value;
                this.RaisePropertyChanged("FaxNumber");
            }
        }

        public EmailURI[] EmailURI
        {
            get =>
                this.emailURIField;
            set
            {
                this.emailURIField = value;
                this.RaisePropertyChanged("EmailURI");
            }
        }
    }
}


