﻿using System.ComponentModel;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class UtilsDvceERPSmrtMtrCrteReqMsg
    {
        private BusinessDocumentMessageHeader messageHeaderField;
        private UtilsDvceERPSmrtMtrCrteReqUtilsDvce utilitiesDeviceField;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public BusinessDocumentMessageHeader MessageHeader
        {
            get =>
                this.messageHeaderField;
            set
            {
                this.messageHeaderField = value;
                this.RaisePropertyChanged("MessageHeader");
            }
        }

        public UtilsDvceERPSmrtMtrCrteReqUtilsDvce UtilitiesDevice
        {
            get =>
                this.utilitiesDeviceField;
            set
            {
                this.utilitiesDeviceField = value;
                this.RaisePropertyChanged("UtilitiesDevice");
            }
        }
    }
}

