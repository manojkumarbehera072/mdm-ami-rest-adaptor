﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class CISAdaptorChild
    {
        public string Parent_UUID { get; set; }
        public string UUID { get; set; }
        public DateTime LogDateTime { get; set; }
        public string Request_Type { get; set; }
    }
}