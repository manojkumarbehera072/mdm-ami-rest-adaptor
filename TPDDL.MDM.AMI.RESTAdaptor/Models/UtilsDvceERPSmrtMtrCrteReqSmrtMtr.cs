﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class UtilsDvceERPSmrtMtrCrteReqSmrtMtr
    {
        private UtilitiesAdvancedMeteringSystemID utilitiesAdvancedMeteringSystemIDField;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public UtilitiesAdvancedMeteringSystemID UtilitiesAdvancedMeteringSystemID
        {
            get =>
                this.utilitiesAdvancedMeteringSystemIDField;
            set
            {
                this.utilitiesAdvancedMeteringSystemIDField = value;
                this.RaisePropertyChanged("UtilitiesAdvancedMeteringSystemID");
            }
        }
    }
}

