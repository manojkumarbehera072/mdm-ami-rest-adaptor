﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class UtilsDvceERPSmrtMtrCrteReqIndivMatlMfrInfo
    {
        private PartyInternalID partyInternalIDField;
        private ProductInternalID partNumberIDField;
        private string serialIDField;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public PartyInternalID PartyInternalID
        {
            get =>
                this.partyInternalIDField;
            set
            {
                this.partyInternalIDField = value;
                this.RaisePropertyChanged("PartyInternalID");
            }
        }

        public ProductInternalID PartNumberID
        {
            get =>
                this.partNumberIDField;
            set
            {
                this.partNumberIDField = value;
                this.RaisePropertyChanged("PartNumberID");
            }
        }

        public string SerialID
        {
            get =>
                this.serialIDField;
            set
            {
                this.serialIDField = value;
                this.RaisePropertyChanged("SerialID");
            }
        }
    }
}

