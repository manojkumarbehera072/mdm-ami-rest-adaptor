﻿using System.ComponentModel;
using System.Xml.Serialization;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class BusinessDocumentMessageID
    {
        private string schemeIDField;
        private string schemeAgencyIDField;
        private string schemeAgencySchemeAgencyIDField;
        private string valueField;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        [XmlAttribute(DataType = "token")]
        public string schemeID
        {
            get =>
                this.schemeIDField;
            set
            {
                this.schemeIDField = value;
                this.RaisePropertyChanged("schemeID");
            }
        }

        public string schemeAgencyID
        {
            get =>
                this.schemeAgencyIDField;
            set
            {
                this.schemeAgencyIDField = value;
                this.RaisePropertyChanged("schemeAgencyID");
            }
        }

        public string schemeAgencySchemeAgencyID
        {
            get =>
                this.schemeAgencySchemeAgencyIDField;
            set
            {
                this.schemeAgencySchemeAgencyIDField = value;
                this.RaisePropertyChanged("schemeAgencySchemeAgencyID");
            }
        }

        public string Value
        {
            get =>
                this.valueField;
            set
            {
                this.valueField = value;
                this.RaisePropertyChanged("Value");
            }
        }
    }
}


