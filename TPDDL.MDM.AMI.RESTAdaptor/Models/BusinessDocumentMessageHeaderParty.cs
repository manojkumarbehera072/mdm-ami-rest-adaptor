﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class BusinessDocumentMessageHeaderParty
    {
        private PartyInternalID internalIDField;
        private PartyStandardID[] standardIDField;
        private BusinessDocumentMessageHeaderPartyContactPerson contactPersonField;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public PartyInternalID InternalID
        {
            get =>
                this.internalIDField;
            set
            {
                this.internalIDField = value;
                this.RaisePropertyChanged("InternalID");
            }
        }

        public PartyStandardID[] StandardID
        {
            get =>
                this.standardIDField;
            set
            {
                this.standardIDField = value;
                this.RaisePropertyChanged("StandardID");
            }
        }

        public BusinessDocumentMessageHeaderPartyContactPerson ContactPerson
        {
            get =>
                this.contactPersonField;
            set
            {
                this.contactPersonField = value;
                this.RaisePropertyChanged("ContactPerson");
            }
        }
    }
}


   