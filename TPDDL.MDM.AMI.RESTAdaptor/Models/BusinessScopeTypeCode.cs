﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class BusinessScopeTypeCode
    {
        private string listIDField;
        private string listVersionIDField;
        private string listAgencyIDField;
        private string listAgencySchemeIDField;
        private string listAgencySchemeAgencyIDField;
        private string valueField;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string listID
        {
            get =>
                this.listIDField;
            set
            {
                this.listIDField = value;
                this.RaisePropertyChanged("listID");
            }
        }

        public string listVersionID
        {
            get =>
                this.listVersionIDField;
            set
            {
                this.listVersionIDField = value;
                this.RaisePropertyChanged("listVersionID");
            }
        }

        public string listAgencyID
        {
            get =>
                this.listAgencyIDField;
            set
            {
                this.listAgencyIDField = value;
                this.RaisePropertyChanged("listAgencyID");
            }
        }

        public string listAgencySchemeID
        {
            get =>
                this.listAgencySchemeIDField;
            set
            {
                this.listAgencySchemeIDField = value;
                this.RaisePropertyChanged("listAgencySchemeID");
            }
        }

        public string listAgencySchemeAgencyID
        {
            get =>
                this.listAgencySchemeAgencyIDField;
            set
            {
                this.listAgencySchemeAgencyIDField = value;
                this.RaisePropertyChanged("listAgencySchemeAgencyID");
            }
        }

        public string Value
        {
            get =>
                this.valueField;
            set
            {
                this.valueField = value;
                this.RaisePropertyChanged("Value");
            }
        }
    }
}

