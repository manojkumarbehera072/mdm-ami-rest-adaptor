﻿using System;
using System.ComponentModel;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class BusinessDocumentMessageHeader
    {
        public BusinessDocumentMessageID idField { get; set; }
        public UUID uUIDField { get; set; }
        public BusinessDocumentMessageID referenceIDField { get; set; }
        public UUID referenceUUIDField { get; set; }
        public DateTime creationDateTimeField { get; set; }
        public bool testDataIndicatorField { get; set; }
        public bool testDataIndicatorFieldSpecified { get; set; }
        public bool reconciliationIndicatorField { get; set; }
        public bool reconciliationIndicatorFieldSpecified { get; set; }
        public string senderBusinessSystemIDField { get; set; }
        public string recipientBusinessSystemIDField { get; set; }
        public BusinessDocumentMessageHeaderParty senderPartyField { get; set; }
        public BusinessDocumentMessageHeaderParty[] recipientPartyField { get; set; }
        public BusinessScope[] businessScopeField { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public BusinessDocumentMessageID ID
        {
            get =>
                this.idField;
            set
            {
                this.idField = value;
                this.RaisePropertyChanged("ID");
            }
        }

        public UUID UUID
        {
            get =>
                this.uUIDField;
            set
            {
                this.uUIDField = value;
                this.RaisePropertyChanged("UUID");
            }
        }

        public BusinessDocumentMessageID ReferenceID
        {
            get =>
                this.referenceIDField;
            set
            {
                this.referenceIDField = value;
                this.RaisePropertyChanged("ReferenceID");
            }
        }

        public UUID ReferenceUUID
        {
            get =>
                this.referenceUUIDField;
            set
            {
                this.referenceUUIDField = value;
                this.RaisePropertyChanged("ReferenceUUID");
            }
        }

        public DateTime CreationDateTime
        {
            get =>
                this.creationDateTimeField;
            set
            {
                this.creationDateTimeField = value;
                this.RaisePropertyChanged("CreationDateTime");
            }
        }

        public bool TestDataIndicator
        {
            get =>
                this.testDataIndicatorField;
            set
            {
                this.testDataIndicatorField = value;
                this.RaisePropertyChanged("TestDataIndicator");
            }
        }

        public bool TestDataIndicatorSpecified
        {
            get =>
                this.testDataIndicatorFieldSpecified;
            set
            {
                this.testDataIndicatorFieldSpecified = value;
                this.RaisePropertyChanged("TestDataIndicatorSpecified");
            }
        }

        public bool ReconciliationIndicator
        {
            get =>
                this.reconciliationIndicatorField;
            set
            {
                this.reconciliationIndicatorField = value;
                this.RaisePropertyChanged("ReconciliationIndicator");
            }
        }

        public bool ReconciliationIndicatorSpecified
        {
            get =>
                this.reconciliationIndicatorFieldSpecified;
            set
            {
                this.reconciliationIndicatorFieldSpecified = value;
                this.RaisePropertyChanged("ReconciliationIndicatorSpecified");
            }
        }

        public string SenderBusinessSystemID
        {
            get =>
                this.senderBusinessSystemIDField;
            set
            {
                this.senderBusinessSystemIDField = value;
                this.RaisePropertyChanged("SenderBusinessSystemID");
            }
        }

        public string RecipientBusinessSystemID
        {
            get =>
                this.recipientBusinessSystemIDField;
            set
            {
                this.recipientBusinessSystemIDField = value;
                this.RaisePropertyChanged("RecipientBusinessSystemID");
            }
        }

        public BusinessDocumentMessageHeaderParty SenderParty
        {
            get =>
                this.senderPartyField;
            set
            {
                this.senderPartyField = value;
                this.RaisePropertyChanged("SenderParty");
            }
        }

        public BusinessDocumentMessageHeaderParty[] RecipientParty
        {
            get =>
                this.recipientPartyField;
            set
            {
                this.recipientPartyField = value;
                this.RaisePropertyChanged("RecipientParty");
            }
        }

        public BusinessScope[] BusinessScope
        {
            get =>
                this.businessScopeField;
            set
            {
                this.businessScopeField = value;
                this.RaisePropertyChanged("BusinessScope");
            }
        }
    }
}


