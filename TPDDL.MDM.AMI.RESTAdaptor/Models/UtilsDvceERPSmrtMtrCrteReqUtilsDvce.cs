﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TPDDL.MDM.AMI.RESTAdaptor.Models
{
    public class UtilsDvceERPSmrtMtrCrteReqUtilsDvce
    {
        private UtilitiesDeviceID idField;
        private DateTime startDateField;
        private DateTime endDateField;
        private string serialIDField;
        private ProductInternalID materialIDField;
        private ProductStandardID_V1 productUniqueItemIDField;
        private UtilsDvceERPSmrtMtrCrteReqIndivMatlMfrInfo individualMaterialManufacturerInformationField;
        private UtilsDvceERPSmrtMtrCrteReqSmrtMtr smartMeterField;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public UtilitiesDeviceID ID
        {
            get =>
                this.idField;
            set
            {
                this.idField = value;
                this.RaisePropertyChanged("ID");
            }
        }

        public DateTime StartDate
        {
            get =>
                this.startDateField;
            set
            {
                this.startDateField = value;
                this.RaisePropertyChanged("StartDate");
            }
        }

        public DateTime EndDate
        {
            get =>
                this.endDateField;
            set
            {
                this.endDateField = value;
                this.RaisePropertyChanged("EndDate");
            }
        }

        public string SerialID
        {
            get =>
                this.serialIDField;
            set
            {
                this.serialIDField = value;
                this.RaisePropertyChanged("SerialID");
            }
        }

        public ProductInternalID MaterialID
        {
            get =>
                this.materialIDField;
            set
            {
                this.materialIDField = value;
                this.RaisePropertyChanged("MaterialID");
            }
        }

        public ProductStandardID_V1 ProductUniqueItemID
        {
            get =>
                this.productUniqueItemIDField;
            set
            {
                this.productUniqueItemIDField = value;
                this.RaisePropertyChanged("ProductUniqueItemID");
            }
        }

        public UtilsDvceERPSmrtMtrCrteReqIndivMatlMfrInfo IndividualMaterialManufacturerInformation
        {
            get =>
                this.individualMaterialManufacturerInformationField;
            set
            {
                this.individualMaterialManufacturerInformationField = value;
                this.RaisePropertyChanged("IndividualMaterialManufacturerInformation");
            }
        }

        public UtilsDvceERPSmrtMtrCrteReqSmrtMtr SmartMeter
        {
            get =>
                this.smartMeterField;
            set
            {
                this.smartMeterField = value;
                this.RaisePropertyChanged("SmartMeter");
            }
        }
    }
}

