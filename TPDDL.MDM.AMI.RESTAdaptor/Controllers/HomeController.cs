﻿using System.Web.Mvc;

namespace TPDDL.MDM.AMI.RESTAdaptor.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
