﻿using System.Web.Http;
using TPDDL.MDM.AMI.RESTAdaptor.Helper;
using TPDDL.MDM.AMI.RESTAdaptor.Models;


namespace TPDDL.MDM.AMI.RESTAdaptor.Controllers
{
    public class AMIController : ApiController
    {

        [Route("api/AMI/UtilitiesDeviceERPSmartMeterCreateRequest_Out")]
        [HttpPost]
        public void UtilitiesDeviceERPSmartMeterCreateRequest_Out([FromBody]UtilsDvceERPSmrtMtrCrteReqMsg utilitiesDeviceERPSmartMeterCreateRequest)
        {
            UtilitiesDeviceERPSmartMeterCreateRequestHelper utilitiesDeviceERPSmartMeterCreate = new UtilitiesDeviceERPSmartMeterCreateRequestHelper();
            utilitiesDeviceERPSmartMeterCreate.UtilitiesDeviceERPSmartMeterCreateRequest_Out(utilitiesDeviceERPSmartMeterCreateRequest);
        }
    }
}

